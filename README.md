# o.melon
![o.melon](src/templates/webapp/views/img/logo.svg)

[o.melon](https://www.npmjs.com/package/o.melon) is the engine used for creating a web application. This repository stores engines core functionality and command line tools.

## Requirements
You need to have installed `npm` and `node` on your system. All base functionality was tested on `npm` 3.10.10 and `node` 6.14.3 versions.

## Installation 
The core functionality of this project can be installed globally and used in command line.

To install it globally, simply run:
```
npm install -g o.melon
```
o.melon uses [Yarn](https://yarnpkg.com/en/) in frontend plugin management, so you need to install it globally as well:
```
npm install -g yarn
```
Also, you will probably need administrative rights for global installation.

## Quick start
You can start the project, by building it and running watch for changes, with one simple command:
```
o.melon -b -w
```
Then you can edit files from `views/` folder without engine restart.

## Commands

| Command                         | Description                                                                                       |
|---------------------------------|---------------------------------------------------------------------------------------------------|
|`o.melon -b, --build`            | This command will build project located in the current folder.                                    |
|`o.melon -w, --watch`            | Watch for source files changes during to the development and rebuild it automatically.            |
|`o.melon -d, --deploy`           | Deploy files, that were built previously.                                                         |
|`o.melon -a, --add <library>`    | Adds library or plugin to the current project.                                                    |
|`o.melon -r, --remove <library>` | Removes library or plugin from the current project.                                               |
|`o.melon -h, --help`             | Display available commands list.                                                                  |
|`o.melon -i, --init [<project>]` | This command will create minimal project structure required for running build and watch commands. |
|`o.melon [command] --prod`       | Use production environment.                                                                       |
|`o.melon [command] --dev`        | Use development environment.                                                                      |

## Troubleshooting
If you get any issues during the installation process or running commands, please check first our [Troubleshooting guide](https://gitlab.com/oshidori/o.melon/wikis/Troubleshooting-guide) first and then you can open an [issue](https://gitlab.com/oshidori/o.melon/issues), if you still have a problem.

Got any questions? Feel free to ask them on Twitter [@oshidori_tech](https://twitter.com/oshidori_tech) or by email [support@oshidori.co](mailto:support@oshidori.co).

## Contributing
Unfortunately, right now, we do not accept any merge requests in [this](https://gitlab.com/oshidori/o.melon/) project. But hey! This is open source project, so you can always make a fork of it. Also, if you have really important feature request, you can always open an [issue](https://gitlab.com/oshidori/o.melon/issues) for this.

## License
This project is licensed under the Apache License, Version 2.0, please see the [LICENSE](https://gitlab.com/oshidori/o.melon/blob/master/LICENSE) file for details.

Copyright &copy; 2018 Oshidori LLC [@oshidori_tech](https://twitter.com/oshidori_tech) [support@oshidori.co](mailto:support@oshidori.co)