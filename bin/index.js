#!/usr/bin/env node
/**
 * @license
 * Copyright 2018 Oshidori LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';


/**
 * @name o.melon Bin
 * @overview Binary wrapper for o.melon engine.
 */


// Define cmd options
const optionDefinitions = [
    {name: 'prod', type: Boolean, description: 'Use production environment.'},
    {name: 'dev', type: Boolean, description: 'Use development environment. It is used by default.'},
    {
        name: 'init',
        alias: 'i',
        type: String,
        description: 'Create project structure. If project type was not provided, then minimal structure will be created. This action is not compatible with other actions.',
        typeLabel: '[<core> | <webapp>]',
        multiple: false
    },
    {name: 'build', alias: 'b', type: Boolean, description: 'Job that build project.'},
    {name: 'watch', alias: 'w', type: Boolean, description: 'Job that serve project for development.'},
    {name: 'deploy', alias: 'd', type: Boolean, description: 'Job that deploy project into AWS.'},
    {
        name: 'add',
        alias: 'a',
        type: String,
        description: 'Search for library by name and add it into the project. This action is not compatible with other actions.',
        typeLabel: '{underline <library>}',
        multiple: false
    },
    {
        name: 'remove',
        alias: 'r',
        type: String,
        description: 'Search for library by name and remove it from the project. This action is not compatible with other actions.',
        typeLabel: '{underline <library>}',
        multiple: false
    },
    {name: 'help', alias: 'h', type: Boolean, description: 'Display this usage guide.'},
];

// Load dependencies
const commandLineUsage = require('command-line-usage');
const commandLineArgs = require('command-line-args');

// Initialize usage and options
const sections = [
    {
        header: 'o.melon.cmd',
        content: 'Command line interface for the o.melon project.'
    },
    {
        header: 'Options',
        optionList: optionDefinitions
    }
];
const usage = commandLineUsage(sections);
let options = {};

try {
    options = commandLineArgs(optionDefinitions, {stopAtFirstUnknown: true});
} catch (err) {
    console.log(usage);
    o.melon.logger.error('Failed to parse command line options.');
    process.exit(1);
}


// Production will be over development
if (options.prod) {
    // Setup production environment
    process.env.NODE_ENV = 'production';
} else if (options.dev) {
    // Setup development environment
    process.env.NODE_ENV = 'development';
}


// Load o.melon core
require('../index');

// Check if o.melon was successfully loaded
if (o && o.melon) {

    // Process provided options
    if (options.help) {
        // Call cmd help
        console.log(usage);

    } else if (options.init) {
        // Init action should be performed first and can't be used in chain with other actions
        o.melon.logger.info('Only ' + o.melon.logger.cmdColors.getInBlue('init') + ' action will be performed, because it is not compatible with other actions.');
        o.melon.init(options.init);

    } else if (options.add) {
        // Add action should be performed first and can't be used in chain with other actions
        o.melon.logger.info('Only ' + o.melon.logger.cmdColors.getInBlue('add') + ' action will be performed, because it is not compatible with other actions.');
        o.melon.add(options.add);

    } else if (options.remove) {
        // Remove action should be performed first and can't be used in chain with other actions
        o.melon.logger.info('Only ' + o.melon.logger.cmdColors.getInBlue('remove') + ' action will be performed, because it is not compatible with other actions.');
        o.melon.remove(options.remove);

    } else if (!Object.keys(options).length || options['_unknown']) {
        // Checks, if options were parsed incorrectly
        console.log(usage);
        o.melon.logger.error('Failed to parse command line options.');
        process.exit(1);

    } else {
        // Execute all rest provided jobs
        for (let jobName in options) {
            // We should ignore dev and prod job names
            if (options.hasOwnProperty(jobName) && ['dev', 'prod'].indexOf(jobName) === -1) {
                o.melon[jobName](options[jobName]);
            }
        }
    }

} else {

    // Trigger critical error otherwise
    console.error("error: Can't initialize the o.melon engine.");
    process.exit(1);

}
