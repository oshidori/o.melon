/**
 * @license
 * Copyright 2018 Oshidori LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';


/**
 * @class Assert
 * @overview Creates o.melon assert.
 * @namespace assert
 * @memberOf o.melon
 */
class Assert {

    /**
     * @method
     * @overview Constructor for the Assert class.
     */
    constructor() {
        // Load assert library
        this.assert = require('assert');
    }

    /**
     * @method
     * @overview Validates, that provided parameter have provided type.
     * @param parameter {any} Variable, that contains parameter to test.
     * @param type {string} String, that contains parameters type.
     * @param parameterName {string} String, that contains parameters name.
     */
    type(parameter, type, parameterName) {
        this.assert.equal(typeof parameter, type, 'Parameter `' + parameterName + '` must be a ' + type + '. Received ' + typeof parameter);
    }

    /**
     * @method
     * @overview Validates, that provided parameter is defined.
     * @param parameter {any} Variable, that contains parameter to test.
     * @param parameterName {string} String, that contains parameters name.
     */
    defined(parameter, parameterName) {
        this.assert.notEqual(typeof parameter, 'undefined', 'Parameter `' + parameterName + '` must be defined');
    }

    /**
     * @method
     * @overview Validates, that provided parameter is not empty.
     * @param parameter {any} Variable, that contains parameter to test.
     * @param parameterName {string} String, that contains parameters name.
     */
    notEmpty(parameter, parameterName) {
        this.assert.ok(parameter.length > 0, 'Parameter `' + parameterName + '` could not be empty');
    }

    /**
     * @method
     * @overview Validates, that provided parameter is inside of the provided subset.
     * @param parameter {any} Variable, that contains parameter to test.
     * @param subset {object} Object, that contains subset of values.
     * @param parameterName {string} String, that contains parameters name.
     */
    in(parameter, subset, parameterName) {
        let passValidation = false;
        let availableValues = [];

        // Check if the subset is an array
        if (typeof subset.indexOf === 'function') {
            passValidation = subset.indexOf(parameter) >= 0;
            availableValues = subset;

            // Check if the subset is an object
        } else if (typeof subset === 'object') {
            for (let value in subset) {
                if (subset.hasOwnProperty(value)) {
                    if (subset[value] === parameter) passValidation = true;
                    availableValues.push(value);
                }
            }
        }

        this.assert.ok(passValidation, 'Parameter `' + parameterName + '` must be one of the [' + availableValues + '] values. Received ' + parameter);
    }

}


// Export Assert class
module.exports = Assert;
