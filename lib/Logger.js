/**
 * @license
 * Copyright 2018 Oshidori LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';


/**
 * @class Logger
 * @overview Creates o.melon logger.
 * @namespace logger
 * @memberOf o.melon
 */
class Logger {

    /**
     * @method
     * @overview Constructor for the Logger class.
     * @param level {string} [env=info] - String that represents log level.
     */
    constructor(level = 'info') {
        // Load Assert library
        const Assert = require('./Assert');

        // Create assert object
        /** @namespace o.melon.logger.assert */
        this.assert = new Assert();

        // Assert constructor parameters
        this.assert.type(level, 'string', 'level');
        this.assert.notEmpty(level, 'level');
        this.assert.in(level, Object.keys(this.logLevels), 'level');


        // Load CMDColors library
        const CMDColors = require('./CMDColors');

        // Create cmdColors instance
        /** @namespace o.melon.logger.cmdColors */
        this.cmdColors = new CMDColors();

        // Default log level is info
        this.logLevel = 2;

        // Set custom log level
        this.level = level;
    }

    /**
     * @method
     * @overview Getter that returns the engine prefix.
     * @returns {string} Engine prefix as a string.
     */
    get enginePrefix() {
        return '{' + this.cmdColors.getInLightGreen('o') + '} '
    }

    /**
     * @method
     * @overview Getter that returns the log levels.
     * @returns {object} Object that contains log levels.
     */
    get logLevels() {
        return {error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5}
    }

    /**
     * @method
     * @overview Setter that set the log level.
     * @param logLevel {string} String, that contains log level.
     */
    set level(logLevel) {
        // Assert parameter
        this.assert.type(logLevel, 'string', 'logLevel');
        this.assert.notEmpty(logLevel, 'logLevel');
        this.assert.in(logLevel, Object.keys(this.logLevels), 'logLevel');

        // Set log level
        this.logLevel = this.logLevels[logLevel];
    }

    /**
     * @method
     * @overview Getter that returns the log level.
     * @return {string} String, that contains log level.
     */
    get level() {
        return Object.keys(this.logLevels).find(logLevelKey => this.logLevels[logLevelKey] === this.logLevel)
    }

    /**
     * @method
     * @overview Check if provided log level is currently enabled.
     * @param logLevel {string} String, that contains log level.
     * @returns {boolean} True, if provided log level is enabled
     */
    isLogLevelEnabled(logLevel) {
        // Assert parameter
        this.assert.type(logLevel, 'string', 'logLevel');
        this.assert.notEmpty(logLevel, 'logLevel');
        this.assert.in(logLevel, Object.keys(this.logLevels), 'logLevel');

        return this.logLevels.hasOwnProperty(logLevel) && this.logLevel >= this.logLevels[logLevel];
    }

    /**
     * @method
     * @overview Outputs silly message into the console.
     * @param msg {any} Message to output.
     */
    silly(msg) {
        // Assert parameter
        this.assert.defined(msg, 'msg');

        if (this.isLogLevelEnabled('silly')) {
            console.log(this.enginePrefix + this.cmdColors.getInLightMagenta('silly:'), msg)
        }
    }

    /**
     * @method
     * @overview Outputs debug message into the console.
     * @param msg {any} Message to output.
     */
    debug(msg) {
        // Assert parameter
        this.assert.defined(msg, 'msg');

        if (this.isLogLevelEnabled('debug')) {
            console.log(this.enginePrefix + this.cmdColors.getInBlue('debug:'), msg)
        }
    }

    /**
     * @method
     * @overview Outputs verbose message into the console.
     * @param msg {any} Message to output.
     */
    verbose(msg) {
        // Assert parameter
        this.assert.defined(msg, 'msg');

        if (this.isLogLevelEnabled('verbose')) {
            console.log(this.enginePrefix + this.cmdColors.getInLightBlue('verbose:'), msg)
        }
    }

    /**
     * @method
     * @overview Outputs info message into the console.
     * @param msg {any} Message to output.
     */
    info(msg) {
        // Assert parameter
        this.assert.defined(msg, 'msg');

        if (this.isLogLevelEnabled('info')) {
            console.log(this.enginePrefix + this.cmdColors.getInGreen('info:'), msg)
        }
    }

    /**
     * @method
     * @overview Outputs warning message into the console.
     * @param msg {any} Message to output.
     */
    warn(msg) {
        // Assert parameter
        this.assert.defined(msg, 'msg');

        if (this.isLogLevelEnabled('warn')) {
            console.log(this.enginePrefix + this.cmdColors.getInYellow('warning:'), msg)
        }
    }

    /**
     * @method
     * @overview Outputs error message into the console.
     * @param msg {any} Message to output.
     */
    error(msg) {
        // Assert parameter
        this.assert.defined(msg, 'msg');

        if (this.isLogLevelEnabled('error')) {
            console.log(this.enginePrefix + this.cmdColors.getInRed('error:'), msg)
        }
    }

    /**
     * @method
     * @overview Notify system on task start.
     * @param taskName {string} Task name.
     */
    notifyOnStart(taskName) {
        // Assert parameters
        this.assert.type(taskName, 'string', 'taskName');
        this.assert.notEmpty(taskName, 'taskName');

        this.info('Running ' + this.cmdColors.getInMagenta(taskName) + ' task ...')
    }

    /**
     * @method
     * @overview Notify system when task is completed.
     * @param taskName {string} Task name.
     */
    notifyOnFinish(taskName) {
        // Assert parameters
        this.assert.type(taskName, 'string', 'taskName');
        this.assert.notEmpty(taskName, 'taskName');

        this.info('The ' + this.cmdColors.getInMagenta(taskName) + ' task was successfully completed!');
    }

    /**
     * @method
     * @overview Notify system on task watching start.
     */
    notifyOnWatchStart() {
        this.info('Now watching ' + this.cmdColors.getInBlue('O.O') + ' for all tasks ...')
    }

    /**
     * @method
     * @overview Notify system when tasks pipe was successfully completed.
     */
    notifyOnFinishAll() {
        this.info(this.cmdColors.getInBlue('The tasks pipe was successfully completed!'))
    }

}


// Export Logger class
module.exports = Logger;
