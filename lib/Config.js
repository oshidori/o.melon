/**
 * @license
 * Copyright 2018 Oshidori LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';


/**
 * @class Config
 * @overview Class that handle configuration presets.
 * @namespace config
 * @memberOf o.melon
 */
class Config {

    /**
     * @method
     * @overview Constructor for the Config class.
     * @param env {string} [env=development] - String that represents current environment.
     */
    constructor(env = process.env.NODE_ENV || 'development') {
        // Load Assert library
        const Assert = require('./Assert');

        // Create assert object
        /** @namespace o.melon.config.assert */
        this.assert = new Assert();

        // Assert constructor parameters
        this.assert.type(env, 'string', 'env');
        this.assert.notEmpty(env, 'env');


        // Load path library
        this.path = require('path');
        const path = this.path;

        // Load fs library
        const fs = require('fs');

        // Set root folder
        this.rootFolder = process.cwd();

        // Set path to config folder
        const pathToConfig = this.rootFolder + '/config/';


        // Check if all required folders exist
        if (fs.existsSync(pathToConfig)) {
            // Load global config
            let globalConfig = require(pathToConfig + 'global');

            // Set current environment
            globalConfig.env = env;

            // Load environment config
            let envConfigName = pathToConfig + 'env/' + globalConfig.env;
            let envConfig = require(envConfigName);

            // Initialize config file with global config
            let config = Object.assign(globalConfig);

            // Read all config files and combine them into one
            fs.readdirSync(pathToConfig).forEach(function (configFile) {
                // Ignore the global presets and the folders
                if (configFile !== 'global.js' && path.extname(configFile) === '.js') {
                    config = Object.assign(config, require(pathToConfig + configFile));
                }
            });

            // Assign environment config
            config = this.mergeDeep(config, envConfig);

            // Create config class property from config property
            for (let property in config) {
                if (config.hasOwnProperty(property)) {
                    this[property] = config[property];
                }
            }
        }
    }

    /**
     * @method
     * @overview Merge two config objects recursively
     * @param config {object} Original config object to merge into
     * @param configToMerge {object} Config object to merge
     * @returns {object} Merged config object
     */
    mergeDeep(config, configToMerge) {
        // Assert parameters
        this.assert.type(config, 'object', 'config');
        this.assert.type(configToMerge, 'object', 'configToMerge');

        // Process all config properties
        for (let property in config) {
            // Skip all properties that's is not in config
            if (config.hasOwnProperty(property) && configToMerge.hasOwnProperty(property)) {
                if (typeof config[property] === 'object' && typeof configToMerge[property] === 'object') {
                    config[property] = this.mergeDeep(config[property], configToMerge[property]);
                } else {
                    config[property] = configToMerge[property];
                }
            }
        }

        // Process all new properties that comes from config to merge
        for (let property in configToMerge) {
            // Process only new properties
            if (configToMerge.hasOwnProperty(property) && !config.hasOwnProperty(property)) {
                config[property] = configToMerge[property];
            }
        }

        return config
    }

    /**
     * @method
     * @overview Returns portion of path to the resulting folder for the build, based on current environment.
     * @returns {string} Portion of path to the folder with build results.
     */
    get resultFolder() {
        return this.envShort + '/'
    }

    /**
     * @method
     * @overview Returns path to the source folder for the build.
     * @returns {string} Path to the folder with the sources.
     */
    getBuildSrcPath() {
        return this.rootFolder + '/' + this.contentPath.src
    }

    /**
     * @method
     * @overview Returns full path to the resulting folder for the build.
     * @param global {boolean} [global=true] Use global path.
     * @returns {string} Full Path to the folder with build results.
     */
    getBuildDistPath(global = true) {
        // Assert parameters
        this.assert.type(global, 'boolean', 'global');

        if (global) {
            return this.rootFolder + '/' + this.contentPath.dist + this.resultFolder
        } else {
            return this.contentPath.dist + this.resultFolder
        }
    }

    /**
     * @method
     * @overview Returns resource URL based on file path.
     * @param filePath {string} Path to the file.
     * @param global {boolean} [global=true] Use global path.
     * @returns {string} Resource URL.
     */
    getResourceURL(filePath, global = true) {
        // Assert parameters
        this.assert.type(filePath, 'string', 'filePath');
        this.assert.notEmpty(filePath, 'filePath');
        this.assert.type(global, 'boolean', 'global');

        let root = '';
        if (this.addRootSlash) root = '/';

        // Perform join and normalize URL path
        return this.path.join(root, this.path.relative(this.getBuildDistPath(global), filePath)).replace(/\\/g, '/');
    }

    /**
     * @method
     * @overview Returns home URL.
     * @returns {string} Home URL
     */
    get homeURL() {
        if (this.webapp.homeURL !== "") {
            return this.webapp.homeURL
        } else {
            return this.webapp.protocol + '://' + this.webapp.hostname + (this.webapp.port ? ':' + this.webapp.port : '')
        }
    }

}


// Export Config class
module.exports = Config;
