/**
 * @license
 * Copyright 2018 Oshidori LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';


/**
 * @class CMDColors
 * @overview Creates o.melon CMD colors helper class.
 * @namespace cmdColors
 * @memberOf o.melon
 */
class CMDColors {

    /**
     * @method
     * @overview Constructor for the CMDColors class.
     */
    constructor() {
        // Load Assert library
        const Assert = require('./Assert');

        // Create assert object
        /** @namespace o.melon.cmdColors.assert */
        this.assert = new Assert();
    }

    /**
     * @method
     * @overview Getter that returns string with code to reset the cmd style
     * @returns {string} Code to reset the cmd style
     */
    get resetStyle() {
        return '\x1b[0m';
    }

    /**
     * @method
     * @overview Returns input message, painted into the black color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInBlack(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[30m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the red color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInRed(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[31m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the green color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInGreen(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[32m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the yellow color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInYellow(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[33m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the blue color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInBlue(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[34m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the magenta color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInMagenta(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[35m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the cyan color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInCyan(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[36m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the white color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInWhite(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[37m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the light gray color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInLightGray(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[90m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the light red color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInLightRed(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[91m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the light green color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInLightGreen(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[92m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the light yellow color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInLightYellow(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[93m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the light blue color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInLightBlue(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[94m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the light magenta color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInLightMagenta(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[95m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the light cyan color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInLightCyan(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[96m' + msg + this.resetStyle;
    }

    /**
     * @method
     * @overview Returns input message, painted into the light white color.
     * @param msg {string} Input message.
     * @returns {string} Colored message.
     */
    getInLightWhite(msg) {
        // Assert parameter
        this.assert.type(msg, 'string', 'msg');

        return '\x1b[97m' + msg + this.resetStyle;
    }

}


// Export CMDColors class
module.exports = CMDColors;
