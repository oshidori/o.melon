/**
 * @license
 * Copyright 2018 Oshidori LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';


/**
 * @name o.melon Core
 * @overview Creates o.melon object globally.
 */


/**
 * @class Melon
 * @overview Class that handle o.melon core functionality.
 * @namespace o.melon
 */
class Melon {

    /**
     * @method
     * @overview Constructor for the Melon class.
     */
    constructor() {
        // Load Assert library
        const Assert = require('./lib/Assert');

        // Create assert object
        /** @namespace o.melon.assert */
        this.assert = new Assert();

        // Read package.json
        const omelonPackageInfo = require(Melon.getRootPath() + '/package');

        // Get basic info about o.melon
        this.name = omelonPackageInfo.name;
        this.version = omelonPackageInfo.version;
        this.description = omelonPackageInfo.description;
        this.author = omelonPackageInfo.author;
        this.license = omelonPackageInfo.license;

        // Load Config library
        const Config = require('./lib/Config');

        // Create config instance
        /** @namespace o.melon.config */
        this.config = new Config();

        // Load Logger library
        const Logger = require('./lib/Logger');

        // Create logger instance
        /** @namespace o.melon.logger */
        this.logger = new Logger(this.config.logLevel);

        // Create gulp instance
        /** @namespace o.melon.gulp */
        this.gulp = require('gulp');

        // Initialize dependencies
        let ejs = require("gulp-ejs");
        let inject = require('gulp-inject');
        let order = require('gulp-order');
        let newer = require('gulp-newer');
        let imagemin = require('gulp-imagemin');
        let htmlminifier = require('gulp-html-minifier');

        let concat = require('gulp-concat');
        let deporder = require('gulp-deporder');
        let stripdebug = require('gulp-strip-debug');
        let uglify = require('gulp-uglify');

        let sass = require('gulp-sass');
        let less = require('gulp-less');
        let postcss = require('gulp-postcss');
        let assets = require('postcss-assets');
        let autoprefixer = require('autoprefixer');
        let mqpacker = require('css-mqpacker');
        let cssnano = require('cssnano');

        let rename = require('gulp-rename');
        let clean = require('gulp-clean');
        let runSequence = require('run-sequence');

        let yarn = require('gulp-yarn');
        let fs = require('fs');

        // Bind o.melon to local variable
        const omelon = this;

        // Register all tasks that o.melon uses

        // Clean build directory
        omelon.gulp.task(omelon.tasks.clear, function () {
            omelon.logger.notifyOnStart(omelon.tasks.clear);

            return omelon.gulp.src(omelon.config.getBuildDistPath() + "*", {read: false})
                .pipe(clean())
                .on('error', function (e) {
                    omelon.logger.error(e)
                })
                .on('end', function () {
                    omelon.logger.notifyOnFinish(omelon.tasks.clear)
                });
        });


        // Build yarn packages for plugins and libs
        omelon.gulp.task(omelon.tasks.installPlugins, function () {
            if (omelon.config.installPlugins) {
                omelon.logger.notifyOnStart(omelon.tasks.installPlugins);

                let yarnLogLevel = '';
                if (!omelon.logger.isLogLevelEnabled('info')) {
                    yarnLogLevel = '--silent';
                } else if (omelon.logger.isLogLevelEnabled('verbose')) {
                    yarnLogLevel = '--verbose';
                }
                omelon.logger.debug('Using yarnLogLevel: ' + yarnLogLevel);

                // Install plugins and libs
                return omelon.gulp.src([omelon.config.getBuildSrcPath() + 'lib/package.json', omelon.config.getBuildSrcPath() + 'lib/yarn.lock'])
                    .pipe(omelon.gulp.dest(omelon.config.getBuildSrcPath() + 'lib'))
                    .pipe(yarn({args: yarnLogLevel}))
                    .on('error', function (e) {
                        omelon.logger.error(e)
                    })
                    .on('end', function () {
                        omelon.logger.notifyOnFinish(omelon.tasks.installPlugins)
                    });
            }
        });


        // Export plugins and libs
        omelon.gulp.task(omelon.tasks.exportPlugins, [omelon.tasks.installPlugins], function () {
            if (omelon.config.installPlugins) {
                omelon.logger.notifyOnStart(omelon.tasks.exportPlugins);

                // Read Yarn config
                const yarn_config = JSON.parse(fs.readFileSync(omelon.config.getBuildSrcPath() + 'lib/package.json', 'utf8'));

                // Add plugins and libs into dist directory
                return omelon.gulp.src(yarn_config['exportRoutes'])
                    .pipe(omelon.gulp.dest(omelon.config.getBuildDistPath() + 'lib/'))
                    .on('data', function (file) {
                        // Add processed file to the cache list
                        if (fs.lstatSync(file.path).isFile()) {
                            const fileToCache = omelon.config.getResourceURL(file.path);
                            if (omelon.config.webapp.misc.filesToCache.indexOf(fileToCache) === -1) {
                                omelon.config.webapp.misc.filesToCache.push(fileToCache);
                            }
                        }
                    })
                    .on('end', function () {
                        omelon.logger.notifyOnFinish(omelon.tasks.exportPlugins)
                    });
            }
        });


        // Image processing
        omelon.gulp.task(omelon.tasks.images, function () {
            omelon.logger.notifyOnStart(omelon.tasks.images);
            let out = omelon.config.getBuildDistPath() + 'img/',
                img = omelon.gulp.src(omelon.config.getBuildSrcPath() + 'img/**/*')
                    .pipe(newer(out));

            if (omelon.config.webapp.minify) {
                omelon.logger.debug('Perform minify in Image task.');

                omelon.logger.debug('ImageMin using interlaced: ' + omelon.config.webapp.img.interlaced);
                omelon.logger.debug('ImageMin using progressive: ' + omelon.config.webapp.img.progressive);
                omelon.logger.debug('ImageMin using optimizationLevel: ' + omelon.config.webapp.img.optimizationLevel);

                img = img.pipe(
                    imagemin({
                         interlaced: omelon.config.webapp.img.interlaced,
                         progressive: omelon.config.webapp.img.progressive,
                         optimizationLevel: omelon.config.webapp.img.optimizationLevel,
                         svgoPlugins: [{removeViewBox: true}],
                         verbose: omelon.logger.isLogLevelEnabled('verbose')
                     }))
                    .on('error', function (e) {
                        omelon.logger.error(e)
                    });
            }

            return img.pipe(omelon.gulp.dest(out))
                .on('data', function (file) {
                    // Add processed file to the cache list
                    if (fs.lstatSync(file.path).isFile()) {
                        const fileToCache = omelon.config.getResourceURL(file.path);
                        if (omelon.config.webapp.misc.filesToCache.indexOf(fileToCache) === -1) {
                            omelon.config.webapp.misc.filesToCache.push(fileToCache);
                        }
                    }
                })
                .on('end', function () {
                    omelon.logger.notifyOnFinish(omelon.tasks.images)
                });
        });


        // Font processing
        omelon.gulp.task(omelon.tasks.fonts, function () {
            omelon.logger.notifyOnStart(omelon.tasks.fonts);
            let out = omelon.config.getBuildDistPath() + 'fonts/';
            return omelon.gulp.src(omelon.config.getBuildSrcPath() + 'fonts/**/*')
                .pipe(newer(out))
                .pipe(omelon.gulp.dest(out))
                .on('data', function (file) {
                    // Add processed file to the cache list
                    if (fs.lstatSync(file.path).isFile()) {
                        const fileToCache = omelon.config.getResourceURL(file.path);
                        if (omelon.config.webapp.misc.filesToCache.indexOf(fileToCache) === -1) {
                            omelon.config.webapp.misc.filesToCache.push(fileToCache);
                        }
                    }
                })
                .on('end', function () {
                    omelon.logger.notifyOnFinish(omelon.tasks.fonts)
                });
        });


        // EJS processing
        omelon.gulp.task(omelon.tasks.ejs, function () {
            omelon.logger.notifyOnStart(omelon.tasks.ejs);

            // Read Yarn config
            let yarn_config = JSON.parse(fs.readFileSync(omelon.config.getBuildSrcPath() + 'lib/package.json', 'utf8'));

            let plugins = omelon.gulp.src(
                [omelon.config.getBuildDistPath() + 'lib/**/*.js', omelon.config.getBuildDistPath() + 'lib/**/*.css'],
                {read: false}  // Don't read files, just extract paths
            ).pipe(order(yarn_config['order']));

            let out = omelon.config.getBuildDistPath(),
                page = omelon.gulp.src([omelon.config.getBuildSrcPath() + 'ejs/pages/**/*.ejs'])
                    .pipe(ejs({
                        global: global,
                        om: omelon.config
                    }, {}, {ext: omelon.config.webapp.ejs.ext}))
                    .on('error', function (e) {
                        omelon.logger.error(e);
                        page.end()
                    });

            if (omelon.config.installPlugins) {
                page = page
                    .pipe(inject(
                        plugins, {
                            ignorePath: omelon.config.getBuildDistPath(false),
                            addRootSlash: omelon.config.addRootSlash,
                            quiet: !omelon.logger.isLogLevelEnabled('verbose')
                        })
                    )
                    .on('error', function (e) {
                        omelon.logger.error(e)
                    });
            }

            // Minify production code
            if (omelon.config.webapp.minify) {
                omelon.logger.debug('Perform minify in EJS task.');
                page = page.pipe(
                    htmlminifier({
                        collapseWhitespace: true,
                        removeAttributeQuotes: true,
                        removeComments: true
                    }));
            }

            return page.pipe(omelon.gulp.dest(out))
                .on('data', function (file) {
                    if (fs.lstatSync(file.path).isFile()) {
                        const resourceFilePath = omelon.config.getResourceURL(file.path);
                        // Extract page path for cache list
                        if (omelon.config.webapp.misc.filesToCache.indexOf(resourceFilePath) === -1) {
                            omelon.config.webapp.misc.filesToCache.push(resourceFilePath);
                        }

                        // Extract page path for page list
                        if (omelon.config.webapp.pages.indexOf(resourceFilePath) === -1) {
                            omelon.config.webapp.pages.push(resourceFilePath);
                        }
                    }
                })
                .on('end', function () {
                    omelon.logger.notifyOnFinish(omelon.tasks.ejs)
                });
        });


        // JavaScript processing
        omelon.gulp.task(omelon.tasks.js, function () {
            omelon.logger.notifyOnStart(omelon.tasks.js);

            let src = [omelon.config.getBuildSrcPath() + 'js/**/*'];

            if (!omelon.config.devMode) {
                omelon.logger.debug('Ignoring js/debug in JS task.');
                src.push('!' + omelon.config.getBuildSrcPath() + 'js/debug/**/*');
            }

            let jsbuild = omelon.gulp.src(src)
                .pipe(deporder())
                .pipe(concat('main.js'));

            if (omelon.config.webapp.minify) {
                omelon.logger.debug('Perform minify in JS task.');
                jsbuild = jsbuild
                    .pipe(stripdebug())
                    .pipe(uglify({mangle: {toplevel: true}}))
                    .pipe(rename({suffix: '.min'}));
            }

            return jsbuild
                .pipe(omelon.gulp.dest(omelon.config.getBuildDistPath() + 'js/'))
                .on('data', function (file) {
                    if (fs.lstatSync(file.path).isFile()) {
                        const fileToCache = omelon.config.getResourceURL(file.path);
                        if (omelon.config.webapp.misc.filesToCache.indexOf(fileToCache) === -1) {
                            omelon.config.webapp.misc.filesToCache.push(fileToCache);
                        }
                    }
                })
                .on('end', function () {
                    omelon.logger.notifyOnFinish(omelon.tasks.js)
                });
        });


        // CSS processing
        omelon.gulp.task(omelon.tasks.css, [omelon.tasks.images, omelon.tasks.fonts], function () {
            omelon.logger.notifyOnStart(omelon.tasks.css);

            let postCssOpts = [
                assets({loadPaths: ['img/', 'font/'], basePath: omelon.config.getBuildSrcPath()}),
                autoprefixer({browsers: ['last 2 versions', '> 2%']}),
                mqpacker
            ];

            if (omelon.config.webapp.minify) {
                omelon.logger.debug('Perform minify in CSS task.');
                postCssOpts.push(cssnano);
            }

            // Init css build
            let cssBuild;

            // Process css by engine
            if (omelon.config.cssEngine === 'sass') {
                cssBuild = omelon.gulp.src(omelon.config.getBuildSrcPath() + 'sass/main.scss')
                    .pipe(sass())
                    .on('error', function (e) {
                        omelon.logger.error(e)
                    })
                    .pipe(postcss(postCssOpts));

            } else if (omelon.config.cssEngine === 'less') {
                cssBuild = omelon.gulp.src(omelon.config.getBuildSrcPath() + 'less/main.less')
                    .pipe(less({relativeUrls: true}))
                    .on('error', function (e) {
                        omelon.logger.error(e)
                    })
                    .pipe(postcss(postCssOpts));

            }

            if (omelon.config.webapp.minify) {
                omelon.logger.debug('Add .min to main.css');
                cssBuild = cssBuild.pipe(rename({suffix: '.min'}));
            }

            return cssBuild
                .pipe(omelon.gulp.dest(omelon.config.getBuildDistPath() + 'css/'))
                .on('data', function (file) {
                    if (fs.lstatSync(file.path).isFile()) {
                        const fileToCache = omelon.config.getResourceURL(file.path);
                        if (omelon.config.webapp.misc.filesToCache.indexOf(fileToCache) === -1) {
                            omelon.config.webapp.misc.filesToCache.push(fileToCache);
                        }
                    }
                })
                .on('end', function () {
                    omelon.logger.notifyOnFinish(omelon.tasks.css)
                });
        });


        // Misc processing
        omelon.gulp.task(omelon.tasks.misc, function () {
            omelon.logger.notifyOnStart(omelon.tasks.misc);

            let misc = omelon.gulp.src([omelon.config.getBuildSrcPath() + 'ejs/misc/**/*'])
                .pipe(ejs({
                              global: global,
                              om: omelon.config
                          }))
                .on('error', function (e) {
                    omelon.logger.error(e);
                    misc.end()
                })
                .on('end', function () {
                    omelon.logger.notifyOnFinish(omelon.tasks.misc)
                });

            // We need to split `dest` from the `ejs`, because it will crash watch task otherwise
            return misc
                .pipe(omelon.gulp.dest(omelon.config.getBuildDistPath()));
        });


        // Build task
        omelon.gulp.task(omelon.tasks.build, function () {
            omelon.logger.notifyOnStart(omelon.tasks.build);
            return runSequence(
                omelon.tasks.clear,
                [omelon.tasks.exportPlugins],
                [omelon.tasks.ejs, omelon.tasks.css, omelon.tasks.js],
                omelon.tasks.misc,
                omelon.tasks.finishAll
            );
        });


        // Finish All tasks
        omelon.gulp.task(omelon.tasks.finishAll, function (callback) {
            omelon.logger.notifyOnFinishAll();
            if (omelon.config.devMode && omelon.webapp && omelon.webapp.client) {
                if (omelon.logger.isLogLevelEnabled('verbose')) {
                    omelon.webapp.client.emit('o.melon.webapp.messages', 'Files got updated');
                }
                if (omelon.config.webapp.sockets.refreshOnUpdate) {
                    omelon.webapp.client.emit('o.melon.webapp.update');
                }
            }
            callback();
        });


        // Watch for any changes
        omelon.gulp.task(omelon.tasks.watch, function () {
            omelon.logger.notifyOnStart(omelon.tasks.watch);

            let watchWithNotify = function (task) {
                return function () {
                    runSequence(task, omelon.tasks.finishAll)
                };
            };

            // Changes in plugins
            omelon.gulp.watch('lib/package.json', {cwd: omelon.config.getBuildSrcPath()}, watchWithNotify(omelon.tasks.build))
                .on('error', function (e) {
                    omelon.logger.error(e)
                });

            // Changes in images
            omelon.gulp.watch('img/**/*', {cwd: omelon.config.getBuildSrcPath()}, watchWithNotify(omelon.tasks.images))
                .on('error', function (e) {
                    omelon.logger.error(e)
                });

            // Changes in fonts
            omelon.gulp.watch('fonts/**/*', {cwd: omelon.config.getBuildSrcPath()}, watchWithNotify(omelon.tasks.fonts))
                .on('error', function (e) {
                    omelon.logger.error(e)
                });

            // Changes in EJS
            omelon.gulp.watch(['ejs/**/*', '!ejs/misc/**/*'], {cwd: omelon.config.getBuildSrcPath()}, watchWithNotify(omelon.tasks.ejs))
                .on('error', function (e) {
                    omelon.logger.error(e)
                });

            // Changes in JavaScript
            omelon.gulp.watch('js/**/*', {cwd: omelon.config.getBuildSrcPath()}, watchWithNotify(omelon.tasks.js))
                .on('error', function (e) {
                    omelon.logger.error(e)
                });

            // Changes in CSS
            omelon.gulp.watch(['less/**/*', 'sass/**/*'], {cwd: omelon.config.getBuildSrcPath()}, watchWithNotify(omelon.tasks.css))
                .on('error', function (e) {
                    omelon.logger.error(e)
                });

            // Changes in Misc
            omelon.gulp.watch('ejs/misc/**/*', {cwd: omelon.config.getBuildSrcPath()}, watchWithNotify(omelon.tasks.misc))
                .on('error', function (e) {
                    omelon.logger.error(e)
                });

            omelon.logger.notifyOnWatchStart();
        });
    }

    /**
     * @method
     * @overview Getter that returns grunt task names.
     * @return {object} Object, that contains task names.
     * @namespace o.melon.tasks
     */
    get tasks() {
        return {
            clear: 'clear',
            installPlugins: 'installPlugins',
            exportPlugins: 'exportPlugins',
            images: 'images',
            fonts: 'fonts',
            misc: 'misc',
            ejs: 'ejs',
            js: 'js',
            css: 'css',
            finishAll: 'finishAll',
            build: 'build',
            watch: 'watch',
            default: 'default',
            deploy: 'deploy',
            init: 'init'
        }
    }

    /**
     * @method
     * @overview Returns whole path to the project root.
     * @returns {string} Path to the project root.
     */
    static getProjectPath() {
        return process.cwd()
    }

    /**
     * @method
     * @overview Returns whole path to the o.melon root.
     * @returns {string} Path to the o.melon root.
     */
    static getRootPath() {
        return __dirname
    }

    /**
     * @method
     * @overview Job that build project.
     */
    build() {
        this.gulp.start(this.tasks.build);
    }

    /**
     * @method
     * @overview Job that serve project for development.
     */
    watch() {
        // Bind o.melon to local variable
        const omelon = this;

        // Run watch task
        omelon.gulp.start(omelon.tasks.watch);

        if (omelon.config.devMode) {
            // Add server run
            let webapp = {};

            webapp.express = require('express');
            webapp.app = webapp.express();
            webapp.server = require(omelon.config.webapp.protocol).createServer(webapp.app);
            webapp.io = require('socket.io')(webapp.server);

            webapp.app.use(webapp.express.static(omelon.config.getBuildDistPath()));

            // Use o.melon logger
            let logger = omelon.logger;
            webapp.app.use(
                function (req, res, next) {
                    logger.verbose('Request URL:', req.originalUrl);
                    next()
                }
            );

            // Handle 404 status
            webapp.app.use(function (req, res, next) {
                res.status(404).redirect(omelon.config.webapp.notFoundPage);
            });

            // Listen on port
            webapp.server.listen(omelon.config.webapp.port, omelon.config.webapp.hostname);

            logger.info(
                logger.cmdColors.getInBlue('Running server on ')
                + omelon.config.webapp.protocol + '://'
                + omelon.config.webapp.hostname
                + ':' + omelon.config.webapp.port
            );

            webapp.io.on('connection', function (client) {
                logger.verbose('Sockets client ' + logger.cmdColors.getInBlue(client.id) + ' connected...');

                client.on('o.melon.webapp.join', function (data) {
                    logger.verbose('Sockets client ' + logger.cmdColors.getInBlue(client.id) + ': ' + data);
                    // Add client into the global webapp
                    webapp.client = client;
                });
            });

            this.webapp = webapp;
        }
    }

    /**
     * @method
     * @overview Job that deploy project into AWS.
     */
    deploy() {
        // Bind o.melon to local variable
        const omelon = this;

        // Load AWS module
        let AWS = require('aws-sdk');

        // Load aws credentials from .aws if key and secret was not provided
        if (!(omelon.config.aws.accessKeyId
            && omelon.config.aws.secretAccessKey)
            && !omelon.config.aws.credentials) {

            // Select aws profile
            let awsProfile = omelon.config.aws.profile ? omelon.config.aws.profile : 'default';

            // Load from .aws settings
            omelon.config.aws.credentials = new AWS.SharedIniFileCredentials({profile: awsProfile});

            omelon.config.aws.accessKeyId = omelon.config.aws.credentials.accessKeyId;
            omelon.config.aws.secretAccessKey = omelon.config.aws.credentials.secretAccessKey;

        } else if (!(omelon.config.aws.accessKeyId
            && omelon.config.aws.secretAccessKey)
            && omelon.config.aws.credentials) {

            omelon.config.aws.accessKeyId = omelon.config.aws.credentials.accessKeyId;
            omelon.config.aws.secretAccessKey = omelon.config.aws.credentials.secretAccessKey;
        }

        let s3 = new AWS.S3({
            accessKeyId: omelon.config.aws.accessKeyId,
            secretAccessKey: omelon.config.aws.secretAccessKey,
            region: omelon.config.aws.region ? omelon.config.aws.region : 'us-east-2'
        });

        let cloudFront = new AWS.CloudFront({
            accessKeyId: omelon.config.aws.accessKeyId,
            secretAccessKey: omelon.config.aws.secretAccessKey,
            region: omelon.config.aws.region ? omelon.config.aws.region : 'us-east-2'
        });

        // Load dependencies
        let awsPublish = require('gulp-awspublish'),
            publisher = awsPublish.create(omelon.config.aws),
            merge = require('merge-stream'),
            dateFormat = require('dateformat');

        // Flag to check if this bucket is new
        let newBucket = true;

        // Notify that task was started
        omelon.logger.notifyOnStart(omelon.tasks.deploy);

        // Deploy on AWS task
        omelon.gulp.task(omelon.tasks.deploy, function () {
            let distForPublish = omelon.config.getBuildDistPath();

            let gzip = omelon.gulp.src(distForPublish + '**/*.js').pipe(awsPublish.gzip());
            let plain = omelon.gulp.src([distForPublish + '**/*', '!' + distForPublish + '**/*.js']);

            let invalidateCache = {
                DistributionId: omelon.config.aws.cloudFrontDistributionId, /* required */
                InvalidationBatch: {
                    CallerReference: dateFormat(new Date(), 'yyyymmddhhMMss'), // Create reference from timestamp
                    Paths: {
                        Quantity: 0,
                        Items: []
                    }
                }
            };

            return merge(gzip, plain)
                .pipe(publisher.publish())
                .pipe(publisher.sync())
                .on('data', function (file) {
                    let state = '';
                    switch (file.s3.state) {
                        case 'skip':
                            state = omelon.logger.cmdColors.getInWhite(file.s3.state);
                            break;
                        case 'create':
                            state = omelon.logger.cmdColors.getInGreen(file.s3.state);
                            break;
                        case 'update':
                            state = omelon.logger.cmdColors.getInMagenta(file.s3.state);
                            break;
                        case 'delete':
                            state = omelon.logger.cmdColors.getInRed(file.s3.state);
                            break;
                        default:
                            state = omelon.logger.cmdColors.getInWhite(file.s3.state);
                    }

                    omelon.logger.info('File ' + omelon.logger.cmdColors.getInBlue(file.s3.path) + ': ' + state);

                    if (['create', 'update', 'delete'].indexOf(file.s3.state) > -1) {
                        // Push file into invalidation if it was changed
                        invalidateCache.InvalidationBatch.Paths.Items.push('/' + file.s3.path);
                    }
                })
                .on('error', function (e) {
                    omelon.logger.error(e)
                })
                .on('end', function () {
                    // Get invalidation length
                    let invalidationLength = invalidateCache.InvalidationBatch.Paths.Items.length;

                    // Process invalidation if we have changed files
                    if (!newBucket && invalidationLength > 0 && omelon.config.aws.cloudFrontDistributionId) {
                        // Set paths quantity
                        invalidateCache.InvalidationBatch.Paths.Quantity = invalidationLength;

                        // Perform invalidation
                        cloudFront.createInvalidation(invalidateCache, function (err, data) {
                            if (err) {
                                omelon.logger.error(err);
                            } else {
                                omelon.logger.info('File invalidation was added');
                                omelon.logger.verbose(data);

                                omelon.logger.notifyOnFinish(omelon.tasks.deploy);
                            }
                        });
                    } else {
                        omelon.logger.notifyOnFinish(omelon.tasks.deploy);
                    }
                });
        });

        // Check if bucket exist
        s3.listBuckets(function (err, data) {
            if (err) {
                omelon.logger.error(err);
            } else {
                // Check if bucket already exist
                data.Buckets.forEach(function (bucket) {
                    if (bucket.Name === omelon.config.aws.params.Bucket) newBucket = false;
                });

                if (newBucket) {
                    // Create new bucket
                    s3.createBucket({Bucket: omelon.config.aws.params.Bucket}, function (err, data) {
                        if (err) {
                            omelon.logger.error(err);
                        } else {
                            omelon.logger.info('Bucket ' + omelon.logger.cmdColors.getInBlue(omelon.config.aws.params.Bucket) + ' was ' + omelon.logger.cmdColors.getInGreen('created'));
                            omelon.logger.info(data);

                            // Run Deploy task on the new bucket
                            omelon.gulp.start(omelon.tasks.deploy);
                        }
                    });
                } else {
                    // Run Deploy task
                    omelon.gulp.start(omelon.tasks.deploy);
                }
            }
        });
    }

    /**
     * @method
     * @overview Job that initialize o.melon project by type.
     * @param {string} [type] Type of the project to initialize
     */
    init(type) {
        // Assert parameter
        this.assert.type(type, 'string', 'type');
        this.assert.in(type, ['core', 'webapp'], 'type');

        // Bind o.melon to local variable
        const omelon = this;

        // Load FS library
        let fs = require('fs');

        // Load gulp conflict
        let conflict = require('gulp-conflict');

        // Init templates to apply
        let templates = [];

        // Validate that template exist for provided type and type is not core
        if (fs.readdirSync(__dirname + '/src/templates/').indexOf(type) !== -1 && type !== 'core') {
            templates.push(__dirname + '/src/templates/' + type + '/**/*');
        }

        // Core template should be always the last one
        templates.push(__dirname + '/src/templates/core/**/*');

        // Add init task
        omelon.gulp.task(omelon.tasks.init, function () {
            omelon.logger.notifyOnStart(omelon.tasks.init);

            return omelon.gulp.src(templates)
                .pipe(conflict(Melon.getProjectPath(), {skipAll: true}))
                .pipe(omelon.gulp.dest(Melon.getProjectPath()))
                .on('end', function () {
                    omelon.logger.notifyOnFinish(omelon.tasks.init)
                });
        });

        // Run Init task
        omelon.gulp.start(omelon.tasks.init);
    }

    /**
     * @method
     * @overview Search for library and add it into the project.
     * @param {string} libraryName Library name to search.
     */
    add(libraryName) {
        // Assert parameter
        this.assert.type(libraryName, 'string', 'libraryName');
        this.assert.notEmpty(libraryName, 'libraryName');

        // Bind local logger
        const logger = this.logger;

        // Check if library name is set
        if (typeof libraryName === 'undefined') {
            logger.error('Library name should be provided.');
            process.exit(1);
        }

        // Load path library
        const path = require('path');

        // Route to libraries root
        let libPackageRoot = path.resolve(this.config.getBuildSrcPath(), 'lib/');

        // Route to libraries package file
        let libPackagePath = path.resolve(this.config.getBuildSrcPath(), 'lib/package.json');

        // Export route root
        let exportRouteRoot = path.resolve(this.config.getBuildSrcPath(), 'lib/node_modules/');

        // Load FS library
        let fs = require('fs');

        // Initialize minimal lib package object
        let libPackage = {
            'description': 'This is a Yarn package file, designed to manage front-end plugins and libs.',
            'exportRoutes': [],
            'order': ['**'],
            'dependencies': {}
        };

        // Read current lib package if it's exists
        if (fs.existsSync(libPackagePath)) {
            libPackage = JSON.parse(fs.readFileSync(libPackagePath));
        }

        // Check if plugin is already in libraries list
        if (typeof libPackage.dependencies[libraryName] === 'undefined') {
            // Make sure that package file exist
            fs.writeFileSync(libPackagePath, JSON.stringify(libPackage, null, 4));

            logger.info('Running yarn add on package ' + logger.cmdColors.getInBlue(libraryName));

            // Load spawn and execSync
            const {spawn} = require('child_process');
            const {execSync} = require('child_process');

            // Check if yarn handler exist
            let yarnHandler = '';

            // Test yarn.cmd handler
            try {
                if (execSync('yarn.cmd -v', {stdio: 'ignore'}) === null) {
                    yarnHandler = 'yarn.cmd';
                }
            } catch (error) {
                logger.warn('`yarn.cmd` command not found. Checking `yarn`.')
            }

            // Test yarn handler
            if (!yarnHandler) {
                try {
                    if (execSync('yarn -v', {stdio: 'ignore'}) === null) {
                        yarnHandler = 'yarn';
                    }
                } catch (error) {
                    logger.error("Can't find Yarn handler. Please, check if Yarn was installed correctly.");
                    process.exit(1);  // This is critical error, we should exit execution
                }
            }

            const yarn = spawn(yarnHandler, ['--cwd', libPackageRoot, 'add', libraryName])
                .on('error', function (err) {
                    logger.error(err)
                });

            yarn.stdout.on('data', (data) => {
                logger.info(`stdout: ${data}`);
            });

            yarn.stderr.on('data', (data) => {
                logger.error(`stderr: ${data}`);
            });

            yarn.on('close', (code) => {
                // Read updated package file
                libPackage = JSON.parse(fs.readFileSync(libPackagePath));

                let foundInExportRoutes = false;

                // Search library in export routes
                libPackage['exportRoutes'].forEach(function (route) {
                    if (route.includes('node_modules/' + libraryName + '/')) foundInExportRoutes = true;
                });

                // Push the export route for the library
                if (!foundInExportRoutes) {
                    // Load find library
                    let find = require('find');

                    let jsAdded = false;
                    let cssAdded = false;

                    let sourceFiles = find.fileSync(/\.js|\.css$/, this.config.getBuildSrcPath() + 'lib/node_modules/' + libraryName);

                    sourceFiles.forEach(function (sourceFile) {
                        if (sourceFile.endsWith(libraryName + '.js') && !jsAdded) {
                            libPackage['exportRoutes'].push(sourceFile);
                            jsAdded = true;
                        } else if (sourceFile.endsWith(libraryName + '.css') && !cssAdded) {
                            libPackage['exportRoutes'].push(sourceFile);
                            cssAdded = true;
                        }
                    });

                    // If we didn't find dist of the library
                    if (!jsAdded && !cssAdded) {
                        libPackage['exportRoutes'].push(exportRouteRoot + path.sep + libraryName + path.sep + '**');
                    }
                }

                // Save the changes to the package file
                fs.writeFileSync(libPackagePath, JSON.stringify(libPackage, null, 4));

                if (`${code}` === '0') {
                    logger.info('Library ' + logger.cmdColors.getInBlue(libraryName) + ' was added successfully.');
                } else {
                    logger.error('Library add action was failed.');
                }
            });
        } else {
            logger.warn('Library ' + logger.cmdColors.getInBlue(libraryName) + ' is already in place.');
        }
    }

    /**
     * @method
     * @overview Remove library from the project.
     * @param {string} libraryName Library name to search.
     */
    remove(libraryName) {
        // Assert parameter
        this.assert.type(libraryName, 'string', 'libraryName');
        this.assert.notEmpty(libraryName, 'libraryName');

        // Bind local logger
        const logger = this.logger;

        // Check if library name is set
        if (typeof libraryName === 'undefined') {
            logger.error('Library name should be provided.');
            process.exit(1);
        }

        // Load path library
        const path = require('path');

        // Route to libraries root
        let libPackageRoot = path.resolve(this.config.getBuildSrcPath(), 'lib/');

        // Route to libraries package file
        let libPackagePath = path.resolve(this.config.getBuildSrcPath(), 'lib/package.json');

        // Load FS library
        let fs = require('fs');

        // Check if current lib package exists
        if (!fs.existsSync(libPackagePath)) {
            logger.error('Can not find ' + logger.cmdColors.getInBlue(libraryName) + ' library.');
        }

        // Read current lib package
        let libPackage = JSON.parse(fs.readFileSync(libPackagePath));

        // Check if plugin is already in libraries list
        if (typeof libPackage.dependencies[libraryName] !== 'undefined') {
            logger.info('Running yarn remove on package ' + logger.cmdColors.getInBlue(libraryName));

            // Load spawn and execSync
            const {spawn} = require('child_process');
            const {execSync} = require('child_process');

            // Check if yarn handler exist
            let yarnHandler = '';

            // Test yarn.cmd handler
            try {
                if (execSync('yarn.cmd -v', {stdio: 'ignore'}) === null) {
                    yarnHandler = 'yarn.cmd';
                }
            } catch (error) {
                logger.warn('`yarn.cmd` command not found. Checking `yarn`.')
            }

            // Test yarn handler
            if (!yarnHandler) {
                try {
                    if (execSync('yarn -v', {stdio: 'ignore'}) === null) {
                        yarnHandler = 'yarn';
                    }
                } catch (error) {
                    logger.error("Can't find Yarn handler. Please, check if Yarn was installed correctly.");
                    process.exit(1);  // This is critical error, we should exit execution
                }
            }

            // Execute yarn remove
            const yarn = spawn(yarnHandler, ['--cwd', libPackageRoot, 'remove', libraryName])
                .on('error', function (err) {
                    logger.error(err)
                });

            yarn.stdout.on('data', (data) => {
                logger.info(`stdout: ${data}`);
            });

            yarn.stderr.on('data', (data) => {
                logger.error(`stderr: ${data}`);
            });

            yarn.on('close', (code) => {
                // Read updated package file
                libPackage = JSON.parse(fs.readFileSync(libPackagePath));

                // Search library in export routes and remove it
                let exportRoutes = libPackage['exportRoutes'];
                libPackage['exportRoutes'] = exportRoutes.filter(element => !element.includes(path.join('node_modules' + path.sep + libraryName + path.sep)));

                // Search library in order rules and remove it
                let order = libPackage['order'];
                libPackage['order'] = order.filter(element => !element.includes(libraryName));

                // Save the changes to the package file
                fs.writeFileSync(libPackagePath, JSON.stringify(libPackage, null, 4));

                if (`${code}` === '0') {
                    logger.info('Library ' + logger.cmdColors.getInBlue(libraryName) + ' was removed successfully.');
                } else {
                    logger.error('Library remove was failed.');
                }
            });
        } else {
            logger.error('Can not find ' + logger.cmdColors.getInBlue(libraryName) + ' library.');
        }
    }
}


// Create o namespace if it's doesn't exist
if (typeof query === 'undefined') global.o = {};

// Create new o.melon instance
/** @namespace o.melon */
o.melon = new Melon();
